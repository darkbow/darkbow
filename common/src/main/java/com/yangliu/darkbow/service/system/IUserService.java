package com.yangliu.darkbow.service.system;

import com.baomidou.mybatisplus.service.IService;
import com.yangliu.darkbow.model.system.User;

/**
 *
 * User 表数据服务层接口
 *
 */
public interface IUserService extends IService<User> {


}