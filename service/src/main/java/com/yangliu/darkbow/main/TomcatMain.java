package com.yangliu.darkbow.main;

import org.apache.catalina.core.AprLifecycleListener;
import org.apache.catalina.core.JreMemoryLeakPreventionListener;
import org.apache.catalina.core.StandardServer;
import org.apache.catalina.core.ThreadLocalLeakPreventionListener;
import org.apache.catalina.mbeans.GlobalResourcesLifecycleListener;
import org.apache.catalina.startup.Tomcat;
import org.apache.catalina.startup.VersionLoggerListener;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Optional;

/**
 * Created by yangliu on 2017/5/4.
 */
public class TomcatMain {
    public static final Optional<String> port = Optional.ofNullable(System.getenv("PORT"));
    static ClassPathXmlApplicationContext context;
    public static void main(String[] args) throws Exception {
        String contextPath = "/";
        String appBase =  ".";
        Tomcat tomcat = new Tomcat();

        // Add AprLifecycleListener
        StandardServer server = (StandardServer) tomcat.getServer();
        AprLifecycleListener listener = new AprLifecycleListener();
        server.addLifecycleListener(listener);
        VersionLoggerListener versionLoggerListener = new VersionLoggerListener();
        server.addLifecycleListener(versionLoggerListener);
        JreMemoryLeakPreventionListener jreMemoryLeakPreventionListener = new JreMemoryLeakPreventionListener();
        server.addLifecycleListener(jreMemoryLeakPreventionListener);
        GlobalResourcesLifecycleListener globalResourcesLifecycleListener = new GlobalResourcesLifecycleListener();
        server.addLifecycleListener(globalResourcesLifecycleListener);
        ThreadLocalLeakPreventionListener threadLocalLeakPreventionListener = new ThreadLocalLeakPreventionListener();
        server.addLifecycleListener(threadLocalLeakPreventionListener);

        tomcat.setPort(Integer.valueOf(port.orElse("9192") ));
        tomcat.getHost().setAppBase(appBase);
        tomcat.addWebapp(contextPath, appBase);
        tomcat.start();

        context = new ClassPathXmlApplicationContext("classpath:spring/spring.xml");
        context.start();

        tomcat.getServer().await();

    }
}
