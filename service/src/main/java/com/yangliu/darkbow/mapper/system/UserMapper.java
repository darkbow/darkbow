package com.yangliu.darkbow.mapper.system;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.yangliu.darkbow.model.system.User;

/**
 *
 * User 表数据库控制层接口
 *
 */
public interface UserMapper extends BaseMapper<User> {


}