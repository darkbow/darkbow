package com.yangliu.darkbow.service.system.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.yangliu.darkbow.mapper.system.UserMapper;
import com.yangliu.darkbow.model.system.User;
import com.yangliu.darkbow.service.system.IUserService;



/**
 *
 * User 表数据服务层接口实现类
 *
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {


}